package com.foreach.across.samples.valentine.business;

import com.foreach.across.samples.valentine.app.business.Contact;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Arne Vandamme
 */
public class TestContact
{
	@Test
	public void lookupKeySetting() {
		assertEquals( "john", contact( "John", "" ) );
		assertEquals( "doe", contact( "", "Doe" ) );
		assertEquals( "freddymercury", contact( "Freddy", "Mercury" ) );
		assertEquals( "freijanunez", contact( "Freïja", "Nuñez" ) );
		assertEquals( "jeanclaudevandamme", contact( "Jean-Claude", "Van Damme" ) );
	}

	private String contact( String firstName, String lastName ) {
		Contact c = new Contact();
		c.setFirstName( firstName );
		c.setLastName( lastName );

		return c.getLookupKey();
	}
}
