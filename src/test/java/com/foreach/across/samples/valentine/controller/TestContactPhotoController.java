package com.foreach.across.samples.valentine.controller;

import com.foreach.across.samples.valentine.app.business.Contact;
import com.foreach.across.samples.valentine.app.business.Team;
import com.foreach.across.samples.valentine.app.controllers.ContactPhotoController;
import com.foreach.across.samples.valentine.app.repositories.ContactRepository;
import com.foreach.imageserver.client.ImageServerClient;
import com.foreach.imageserver.dto.ImageTypeDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ModelMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;

/**
 * @author Arne Vandamme
 */
public class TestContactPhotoController
{
	@Mock
	private ContactRepository contactRepository;

	@Mock
	private ImageServerClient imageServerClient;

	private ContactPhotoController controller;

	@Before
	public void setupMocks() {
		MockitoAnnotations.initMocks( this );
		controller = new ContactPhotoController( imageServerClient, contactRepository, "" );
	}

	@Test
	public void generatedLookupKeys() {
		assertLookups( "Jean-Claude.Van damme", "jeanclaudevandamme", "vandammejeanclaude" );
		assertLookups( "Van.Damme.Jean-Claude", "vandammejeanclaude", "jeanclaudevandamme" );
		assertLookups( "freddy.mercury", "freddymercury", "mercuryfreddy" );
		assertLookups( "André The Giant", "andrethegiant", null );
	}

	private void assertLookups( String path, String initial, String fallback ) {
		reset( contactRepository );
		ModelMap model = new ModelMap();
		controller.showPhotoForContact( path, model );

		verify( contactRepository ).findOneByLookupKey( initial );
		if ( fallback == null ) {
			verifyNoMoreInteractions( contactRepository );
		}
		else {
			verify( contactRepository ).findOneByLookupKey( fallback );
		}
	}

	@Test
	public void initialLookupWorks() {
		Contact c = new Contact();
		c.setFirstName( "John" );
		c.setLastName( "Doe" );
		Team team = new Team();
		team.setName( "John's team" );
		team.setImageKey( "team-john" );
		c.setTeam( team );

		when( contactRepository.findOneByLookupKey( "johndoe" ) ).thenReturn( c );
		when( imageServerClient.imageUrl( "team-john", "default", 700, 0, ImageTypeDto.PNG ) )
				.thenReturn( "john-image" );

		ModelMap model = new ModelMap();
		controller.showPhotoForContact( "John.Doe", model );

		verify( contactRepository, never() ).findOneByLookupKey( "doejohn" );

		assertSame( c, model.get( "contact" ) );
		assertSame( team, model.get( "team" ) );
		assertEquals( "john-image", model.get( "imageUrl" ) );
	}

	@Test
	public void fallbackyLookupWorks() {
		Contact c = new Contact();
		c.setFirstName( "John" );
		c.setLastName( "Doe" );
		Team team = new Team();
		team.setName( "John's team" );
		team.setImageKey( "team-john" );
		c.setTeam( team );

		when( contactRepository.findOneByLookupKey( "doejohn" ) ).thenReturn( c );
		when( imageServerClient.imageUrl( "team-john", "default", 700, 450, ImageTypeDto.PNG ) )
				.thenReturn( "john-image" );

		ModelMap model = new ModelMap();
		controller.showPhotoForContact( "John.Doe", model );

		verify( contactRepository ).findOneByLookupKey( "johndoe" );

		assertSame( c, model.get( "contact" ) );
		assertSame( team, model.get( "team" ) );
		assertEquals( "john-image", model.get( "imageUrl" ) );
	}
}
