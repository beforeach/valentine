package com.foreach.across.samples.valentine.jobs;

import com.foreach.across.samples.valentine.app.business.Contact;
import com.foreach.across.samples.valentine.app.jobs.FieldsetToContactMapper;
import com.foreach.across.samples.valentine.app.repositories.ContactRepository;
import com.foreach.across.samples.valentine.app.repositories.TeamRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Arne Vandamme
 */
public class TestFieldsetToContactImporter
{
	@Mock
	private ContactRepository contactRepository;

	@Mock
	private TeamRepository teamRepository;

	private FieldsetToContactMapper importer;

	@Before
	public void setupMocks() {
		MockitoAnnotations.initMocks( this );
		importer = new FieldsetToContactMapper();
	}

	@Test
	public void importNewContact() throws BindException {
		FieldSet fieldSet = mock( FieldSet.class );
		when( fieldSet.readString( "teamName" ) ).thenReturn( "team-one" );
		when( fieldSet.readString( "contactName" ) ).thenReturn( "Jean-Claude Van Damme" );

		Contact contact = importer.mapFieldSet( fieldSet );
		assertNotNull( contact );
	}
}
