package com.foreach.across.samples.valentine.app.jobs;

import com.foreach.across.samples.valentine.app.business.Contact;
import com.foreach.across.samples.valentine.app.business.Team;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;


/**
 * Converts column values into a dummy contact instance.
 *
 * @author Arne Vandamme
 * @see ContactImportProcessor
 */
public class FieldsetToContactMapper implements FieldSetMapper<Contact>
{
	@Override
	public Contact mapFieldSet( FieldSet fieldSet ) throws BindException {
		Team team = new Team();
		team.setName( fieldSet.readString( "teamName" ) );

		String name = StringUtils.trim( fieldSet.readString( "contactName" ) );

		Contact contact = new Contact();
		contact.setTeam( team );
		contact.setFirstName( StringUtils.substringBefore( name, " " ) );
		contact.setLastName( StringUtils.substringAfter( name, " " ) );

		return contact;
	}
}
