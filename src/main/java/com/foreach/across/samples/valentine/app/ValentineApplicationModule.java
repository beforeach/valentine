package com.foreach.across.samples.valentine.app;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossRole;
import com.foreach.across.core.context.AcrossModuleRole;
import com.foreach.across.modules.hibernate.provider.HibernatePackageConfiguringModule;
import com.foreach.across.modules.hibernate.provider.HibernatePackageRegistry;
import org.springframework.core.Ordered;

/**
 * @author Arne Vandamme
 */
@AcrossRole(value = AcrossModuleRole.APPLICATION, order = Ordered.LOWEST_PRECEDENCE)
public class ValentineApplicationModule extends AcrossModule implements HibernatePackageConfiguringModule
{
	public static final String NAME = "ValentineApplicationModule";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getResourcesKey() {
		return "valentine";
	}

	@Override
	public String getDescription() {
		return "Core Valentine application logic";
	}

	public void configureHibernatePackage( HibernatePackageRegistry hibernatePackage ) {
		hibernatePackage.addPackageToScan( getClass() );
	}

}
