package com.foreach.across.samples.valentine.app.repositories;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import com.foreach.across.samples.valentine.app.business.Contact;

/**
 * @author Arne Vandamme
 */
public interface ContactRepository extends IdBasedEntityJpaRepository<Contact>
{
	Contact findOneByLookupKey( String lookupKey );
}
