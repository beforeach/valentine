package com.foreach.across.samples.valentine.app.controllers;

import com.foreach.across.modules.web.resource.WebResource;
import com.foreach.across.modules.web.resource.WebResourceRegistry;
import com.foreach.across.samples.valentine.app.business.Contact;
import com.foreach.across.samples.valentine.app.repositories.ContactRepository;
import com.foreach.imageserver.client.ImageServerClient;
import com.foreach.imageserver.dto.ImageTypeDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author Arne Vandamme
 */
@Controller
public class ContactPhotoController
{
	private final ImageServerClient imageServerClient;
	private final ContactRepository contactRepository;
	private final String applicationUrl;

	@Autowired
	public ContactPhotoController( ImageServerClient imageServerClient,
	                               ContactRepository contactRepository,
	                               @Value("${application.url}") String applicationUrl ) {
		this.imageServerClient = imageServerClient;
		this.contactRepository = contactRepository;
		this.applicationUrl = applicationUrl;
	}

	@ModelAttribute
	protected void registerWebResources( WebResourceRegistry resourceRegistry ) {
		resourceRegistry.addWithKey( WebResource.CSS, "main", "/static/valentine/css/main.css", WebResource.VIEWS );
		resourceRegistry.addWithKey( WebResource.CSS, "google-fonts",
		                             "https://fonts.googleapis.com/css?family=PT+Sans:400,700", WebResource.EXTERNAL );
		resourceRegistry.addWithKey( WebResource.CSS, "font-awesome",
		                             "https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css",
		                             WebResource.EXTERNAL );
	}

	@RequestMapping(value = "/{contactPath:.+}", method = RequestMethod.GET)
	public String showPhotoForContact( @PathVariable("contactPath") String contactPath, ModelMap model ) {
		String initialLookupKey = generateInitialLookupKey( contactPath );
		Contact contact = contactRepository.findOneByLookupKey( initialLookupKey );

		if ( contact == null ) {
			String fallbackLookupKey = generateFallbackLookupKey( contactPath );
			if ( !StringUtils.equals( initialLookupKey, fallbackLookupKey ) ) {
				contact = contactRepository.findOneByLookupKey( fallbackLookupKey );
			}
		}

		if ( contact != null ) {
			model.addAttribute( "sharePage", contact.getTeam().isSharingActive() );
			model.addAttribute( "contact", contact );
			model.addAttribute( "team", contact.getTeam() );
			model.addAttribute( "shareUrl", buildShareUrl( contact ) );
			model.addAttribute( "shareTitle", buildShareTitle( contact ) );
			model.addAttribute(
					"imageUrl",
					imageServerClient.imageUrl( contact.getTeam().getImageKey(), "default", 700, 0, ImageTypeDto.PNG )
			);
		}
		else {
			model.addAttribute( "sharePage", false );
			model.addAttribute(
					"imageUrl",
					imageServerClient.imageUrl( "contact-not-found", "default", 700, 0, ImageTypeDto.PNG )
			);
		}

		return "th/valentine/photo";
	}

	private String generateInitialLookupKey( String path ) {
		return Contact.generateLookupKey( path, "" );
	}

	private String generateFallbackLookupKey( String path ) {
		String firstName = StringUtils.substringAfterLast( path, "." );
		String lastName = StringUtils.substringBeforeLast( path, "." );

		return Contact.generateLookupKey( firstName, lastName );
	}

	private String buildShareUrl( Contact contact ) {
		return UriComponentsBuilder.fromHttpUrl( applicationUrl )
		                           .path( "/" ).path( contact.getLookupKey() )
		                           .toUriString();
	}

	private String buildShareTitle( Contact contact ) {
		return "@beforeach wishing " + contact.getFirstName() + " a Happy Valentine's Day";
	}
}
