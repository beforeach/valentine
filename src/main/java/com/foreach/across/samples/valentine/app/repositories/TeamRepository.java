package com.foreach.across.samples.valentine.app.repositories;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import com.foreach.across.samples.valentine.app.business.Team;

/**
 * @author Arne Vandamme
 */
public interface TeamRepository extends IdBasedEntityJpaRepository<Team>
{
	Team findOneByName( String name );
}