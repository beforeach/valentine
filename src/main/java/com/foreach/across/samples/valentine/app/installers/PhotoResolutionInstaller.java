package com.foreach.across.samples.valentine.app.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.imageserver.core.business.ImageContext;
import com.foreach.imageserver.core.business.ImageResolution;
import com.foreach.imageserver.core.business.ImageType;
import com.foreach.imageserver.core.services.ImageContextService;
import com.foreach.imageserver.core.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.util.Collections;

/**
 * @author Arne Vandamme
 */
@Order(1602091156)
@Installer(description = "Add a default resolution of 700x0 for photo images.", phase = InstallerPhase.AfterModuleBootstrap)
public class PhotoResolutionInstaller
{
	@Autowired
	private ImageService imageService;

	@Autowired
	private ImageContextService imageContextService;

	@InstallerMethod
	public void registerPhotoResolution() {
		ImageContext context = imageContextService.getByCode( "default" );

		ImageResolution imageResolution = new ImageResolution();
		imageResolution.setName( "photo" );
		imageResolution.setContexts( Collections.singleton( context ) );
		imageResolution.setPregenerateVariants( true );
		imageResolution.setWidth( 700 );
		imageResolution.setHeight( 0 );
		imageResolution.setAllowedOutputTypes( Collections.singleton( ImageType.PNG ) );

		imageService.saveImageResolution( imageResolution );
	}
}
