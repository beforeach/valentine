package com.foreach.across.samples.valentine.app.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.installers.AcrossLiquibaseInstaller;
import org.springframework.core.annotation.Order;

/**
 * @author Arne Vandamme
 */
@Order(1602081434)
@Installer(description = "Installs core Valentine schema")
public class ValentineSchemaInstaller extends AcrossLiquibaseInstaller
{
}
