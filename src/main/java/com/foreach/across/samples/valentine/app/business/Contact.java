package com.foreach.across.samples.valentine.app.business;

import com.foreach.across.modules.hibernate.business.SettableIdBasedEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.regex.Pattern;

/**
 * @author Arne Vandamme
 */
@Entity
@Table(name = "val_contact")
public class Contact extends SettableIdBasedEntity<Contact>
{
	private static final long serialVersionUID = 4401787815278930017L;

	@Id
	@GeneratedValue(generator = "seq_val_contact_id")
	@GenericGenerator(
			name = "seq_val_contact_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_val_contact_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@NotBlank
	@Size(max = 255)
	@Column(nullable = false, name = "first_name")
	private String firstName;

	@NotBlank
	@Size(max = 255)
	@Column(nullable = false, name = "last_name")
	private String lastName;

	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "team_id")
	private Team team;

	@NotBlank
	@Size(max = 255)
	@Column(nullable = false, name = "lookup_key")
	private String lookupKey;

	@Override
	public void setId( Long id ) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName( String firstName ) {
		this.firstName = firstName;
		buildLookupKey();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName( String lastName ) {
		this.lastName = lastName;
		buildLookupKey();
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam( Team team ) {
		this.team = team;
	}

	public String getName() {
		return getFirstName() + " " + getLastName();
	}

	public String getLookupKey() {
		return lookupKey;
	}

	private void buildLookupKey() {
		lookupKey = generateLookupKey( getFirstName(), getLastName() );
	}

	public static String generateLookupKey( String firstName, String lastName ) {
		String lookupKey = StringUtils.stripAccents( StringUtils.lowerCase( firstName + lastName ) );
		Pattern pattern = Pattern.compile( "[-\\s_/\\.,]" );

		return pattern.matcher( lookupKey ).replaceAll( "" );
	}
}
