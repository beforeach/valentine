package com.foreach.across.samples.valentine;

import com.foreach.across.config.AcrossApplication;
import com.foreach.across.database.support.HikariDataSourceHelper;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.applicationinfo.ApplicationInfoModule;
import com.foreach.across.modules.debugweb.DebugWebModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.filemanager.FileManagerModule;
import com.foreach.across.modules.logging.LoggingModule;
import com.foreach.across.modules.logging.LoggingModuleSettings;
import com.foreach.across.modules.logging.request.RequestLoggerConfiguration;
import com.foreach.across.modules.spring.batch.SpringBatchModule;
import com.foreach.across.modules.user.UserModule;
import com.foreach.across.modules.user.UserModuleSettings;
import com.foreach.across.samples.valentine.app.ValentineApplicationModule;
import com.foreach.imageserver.admin.ImageServerAdminWebModule;
import com.foreach.imageserver.core.ImageServerCoreModule;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.DispatcherType;
import javax.sql.DataSource;
import java.util.Arrays;

@AcrossApplication(
		modules = { AdminWebModule.NAME,
		            DebugWebModule.NAME,
		            UserModule.NAME,
		            EntityModule.NAME,
		            ApplicationInfoModule.NAME,
		            SpringBatchModule.NAME,
		            FileManagerModule.NAME,
		            ValentineApplicationModule.NAME
		}
)
@PropertySource(value = "classpath:build.properties", ignoreResourceNotFound = true)
public class ValentineApplication extends WebMvcConfigurerAdapter
{
	@Bean
	public DataSource acrossDataSource( ConfigurableEnvironment environment ) {
		return new HikariDataSource( HikariDataSourceHelper.create( "valentine", environment ) );
	}

	@Bean
	public LoggingModule loggingModule() {
		LoggingModule loggingModule = new LoggingModule();
		RequestLoggerConfiguration requestLoggerConfiguration = RequestLoggerConfiguration.allRequests();
		requestLoggerConfiguration.setExcludedPathPatterns(
				Arrays.asList( "/across/**", "/admin/**", "/debug/**", "/robots.txt" )
		);
		loggingModule.setProperty( LoggingModuleSettings.REQUEST_LOGGER_CONFIGURATION, requestLoggerConfiguration );

		return loggingModule;
	}

	@Bean
	public ImageServerCoreModule imageServerCoreModule() {
		return new ImageServerCoreModule();
	}

	@Bean
	public ImageServerAdminWebModule imageServerAdminModule() {
		return new ImageServerAdminWebModule();
	}

	@Profile("dev")
	@Bean
	public UserModule userModule() {
		UserModule userModule = new UserModule();
		userModule.setProperty( UserModuleSettings.PASSWORD_ENCODER, NoOpPasswordEncoder.getInstance() );
		return userModule;
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();

		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding( "UTF-8" );
		characterEncodingFilter.setForceEncoding( true );

		registrationBean.setFilter( characterEncodingFilter );
		registrationBean.addUrlPatterns( "/*" );
		registrationBean.setDispatcherTypes( DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.ERROR );
		return registrationBean;
	}

	@Override
	public void addResourceHandlers( ResourceHandlerRegistry registry ) {
		registry.addResourceHandler( "/robots.txt" ).addResourceLocations( "classpath:/app/" );
		registry.addResourceHandler( "/favicon.ico" ).addResourceLocations( "classpath:/views/static/valentine/images/" );
	}

	public static void main( String[] args ) {
		SpringApplication.run( ValentineApplication.class, args );
	}
}
