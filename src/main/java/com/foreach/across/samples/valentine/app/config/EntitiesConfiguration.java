package com.foreach.across.samples.valentine.app.config;

import com.foreach.across.core.annotations.Event;
import com.foreach.across.core.annotations.EventName;
import com.foreach.across.modules.adminweb.AdminWeb;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiFactory;
import com.foreach.across.modules.bootstrapui.elements.GlyphIcon;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.views.EntityListView;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.processors.WebViewProcessorAdapter;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.web.events.BuildTemplateWebResourcesEvent;
import com.foreach.across.modules.web.resource.WebResource;
import com.foreach.across.modules.web.ui.ViewElementBuilder;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;
import com.foreach.across.samples.valentine.app.business.Contact;
import com.foreach.across.samples.valentine.app.business.Team;
import com.foreach.imageserver.client.ImageServerClient;
import com.foreach.imageserver.dto.ImageInfoDto;
import com.foreach.imageserver.dto.ImageTypeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

/**
 * @author Arne Vandamme
 */
@Configuration
@Controller
public class EntitiesConfiguration implements EntityConfigurer
{
	@Event
	protected void registerAdminWebResources( @EventName(AdminWeb.NAME) BuildTemplateWebResourcesEvent resources ) {
		resources.add( WebResource.CSS, "/static/valentine/css/admin.css", WebResource.VIEWS );
	}

	@Override
	public void configure( EntitiesConfigurationBuilder configuration ) {
		configuration.entity( Team.class )
		             .properties().property( "imageKey" )
		             .viewElementBuilder( ViewElementMode.LIST_VALUE, imageKeyViewElementBuilder() ).and().and()
		             .view( EntityListView.SUMMARY_VIEW_NAME )
		             .addProcessor( teamSummaryViewProcessor() )
		             .template( "th/valentine/admin/teamSummary" );

		configuration.entity( Contact.class )
		             .properties().property( "team" )
		             .viewElementBuilder( ViewElementMode.LIST_VALUE, imageKeyViewElementBuilder() );
	}

	@Bean
	public ViewElementBuilder imageKeyViewElementBuilder() {
		return new ImageKeyViewElementBuilder();
	}

	@Bean
	public TeamSummaryViewProcessor teamSummaryViewProcessor() {
		return new TeamSummaryViewProcessor();
	}

	static class ImageKeyViewElementBuilder implements ViewElementBuilder<ContainerViewElement>
	{
		@Autowired
		private BootstrapUiFactory bootstrapUi;

		@Autowired
		private ImageServerClient imageServerClient;

		@Override
		public ContainerViewElement build( ViewElementBuilderContext builderContext ) {
			Team team = retrieveTeam( EntityViewElementUtils.currentEntity( builderContext ) );

			boolean photoExists = imageServerClient.imageExists( team.getImageKey() );

			GlyphIcon icon = new GlyphIcon( photoExists ? GlyphIcon.OK_SIGN : GlyphIcon.EXCLAMATION_SIGN );
			icon.addCssClass( photoExists ? "team-photo-ok" : "team-photo-missing" );

			return bootstrapUi.container()
			                  .add( bootstrapUi.text( team.getImageKey() ) )
			                  .add( icon )
			                  .build( builderContext );
		}

		private Team retrieveTeam( Object entity ) {
			if ( entity instanceof Contact ) {
				return ( (Contact) entity ).getTeam();
			}

			return (Team) entity;
		}
	}

	static class TeamSummaryViewProcessor extends WebViewProcessorAdapter<EntityView>
	{
		@Autowired
		private ImageServerClient imageServerClient;

		@Override
		protected void extendViewModel( EntityView view ) {
			Team team = view.getEntity();

			ImageInfoDto image = imageServerClient.imageInfo( team.getImageKey() );

			view.addAttribute( "image", image );
			if ( image.isExisting() ) {
				view.addAttribute( "imageUrl", imageServerClient.imageUrl( image.getExternalId(), "default", 700, 0,
				                                                           ImageTypeDto.PNG ) );
			}
		}
	}
}
