package com.foreach.across.samples.valentine.app.controllers.admin;

import com.foreach.across.core.annotations.Event;
import com.foreach.across.modules.adminweb.AdminWeb;
import com.foreach.across.modules.adminweb.annotations.AdminWebController;
import com.foreach.across.modules.adminweb.menu.AdminMenuEvent;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiFactory;
import com.foreach.across.modules.bootstrapui.elements.Style;
import com.foreach.across.modules.bootstrapui.elements.TextboxFormElement;
import com.foreach.across.modules.filemanager.services.FileManager;
import com.foreach.across.modules.web.ui.ViewElementBuilderContextImpl;
import com.foreach.across.samples.valentine.app.ValentineApplicationModule;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Arne Vandamme
 */
@AdminWebController
@RequestMapping("/importContacts")
public class ImportContactsController
{
	private static final Logger LOG = LoggerFactory.getLogger( ImportContactsController.class );

	@Autowired
	private AdminWeb adminWeb;

	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private JobRegistry jobRegistry;

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private JobExplorer jobExplorer;

	@Autowired
	private BootstrapUiFactory bootstrapUi;

	@Autowired
	private FileManager fileManager;

	@Event
	public void buildMenu( AdminMenuEvent event ) {
		event.builder()
		     .group( "/valentine", "Valentine" ).order( 1 ).and()
		     .move( "/entities/ValentineApplicationModule", "/valentine" ).and()
		     .item( "/valentine/importContacts", "Import contacts", "/importContacts" ).order(
				Ordered.LOWEST_PRECEDENCE );
	}

	@RequestMapping(method = RequestMethod.GET)
	public String renderImportForm( ModelMap model ) {
		model.addAttribute(
				"uploadForm",
				bootstrapUi.form()
				           .post()
				           .multipart()
				           .add(
						           bootstrapUi.formGroup(
								           bootstrapUi.label( "CSV file to import" ),
								           bootstrapUi.textbox()
								                      .type( new TextboxFormElement.Type( "file" ) )
								                      .controlName( "file" )
								                      .required()
						           )
				           )
				           .add( bootstrapUi.button().submit().style( Style.PRIMARY ).text( "Import contacts" ) )
				           .build( new ViewElementBuilderContextImpl() )
		);

		return "th/valentine/admin/importContacts";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String importCsvContacts( @RequestParam("file") MultipartFile file ) {
		File tempFile = fileManager.createTempFile();

		try (InputStream is = file.getInputStream()) {
			try (OutputStream os = new FileOutputStream( tempFile )) {
				IOUtils.copy( is, os );
			}
		}
		catch ( Exception e ) {
			throw new RuntimeException( e );
		}

		JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
		jobParametersBuilder.addString( "csv-file", "file:" + tempFile.getAbsolutePath() );
		JobParameters jobParameters = jobParametersBuilder.toJobParameters();

		if ( jobRepository.isJobInstanceExists( "importContacts", jobParameters ) ) {
//			JobExecution lastJobExecution = jobRepository.getLastJobExecution( jobName, jobParameters );
//			if ( !lastJobExecution.getExitStatus().equals( ExitStatus.FAILED ) ) {
//				return lastJobExecution.getId();
//			}
		}
		else {

			try {
				Job aatJob = jobRegistry.getJob( ValentineApplicationModule.NAME + ".importContacts" );
				JobExecution execution = jobLauncher.run( aatJob, jobParameters );
			}
			catch ( NoSuchJobException | JobParametersInvalidException | JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException e ) {
				LOG.error( "Can't get or start job", getClass(), e );
			}
		}

		return adminWeb.redirect( "/importContacts" );
	}

}
