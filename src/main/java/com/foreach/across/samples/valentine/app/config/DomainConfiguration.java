package com.foreach.across.samples.valentine.app.config;

import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import com.foreach.across.samples.valentine.app.ValentineApplicationModule;
import com.foreach.across.samples.valentine.app.business.Contact;
import com.foreach.across.samples.valentine.app.controllers.ContactPhotoController;
import com.foreach.across.samples.valentine.app.jobs.ContactImportProcessor;
import com.foreach.across.samples.valentine.app.jobs.FieldsetToContactMapper;
import com.foreach.across.samples.valentine.app.repositories.ContactRepository;
import com.foreach.across.samples.valentine.app.repositories.TeamRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

/**
 * @author Arne Vandamme
 */
@Configuration
@EnableAcrossJpaRepositories(basePackageClasses = ValentineApplicationModule.class)
@ComponentScan(basePackageClasses = ContactPhotoController.class)
public class DomainConfiguration
{
	@Bean
	public Job importContactsJob( JobBuilderFactory jobs, Step step ) {
		return jobs.get( "importContacts" )
		           .incrementer( new RunIdIncrementer() )
		           .flow( step )
		           .end()
		           .build();
	}

	@Bean
	public Step step1( StepBuilderFactory stepBuilderFactory,
	                   ItemStreamReader<Contact> reader,
	                   ContactImportProcessor contactImportProcessor ) {
		return stepBuilderFactory.get( "insertContact" )
		                         .<Contact, String>chunk( 50 )
		                         .reader( reader )
		                         .processor( contactImportProcessor )
		                         .build();
	}

	@Bean
	@JobScope
	public ItemStreamReader<Contact> reader( FieldsetToContactMapper fieldsetToContactMapper,
	                                         @Value("#{jobParameters['csv-file']}") Resource csvFile ) {
		FlatFileItemReader<Contact> reader = new FlatFileItemReader<>();
		reader.setEncoding( "UTF-8" );
		reader.setResource( csvFile );
		reader.setLineMapper(
				new DefaultLineMapper<Contact>()
				{{
						setLineTokenizer( new DelimitedLineTokenizer()
						{{
								setNames( new String[] { "teamName", "contactName" } );
							}} );
						setFieldSetMapper( fieldsetToContactMapper );
					}} );
		return reader;
	}

	@Bean
	public FieldsetToContactMapper fieldsetToContactMapper() {
		return new FieldsetToContactMapper();
	}

	@Bean
	public ContactImportProcessor contactImportProcessor( ContactRepository contactRepository,
	                                                      TeamRepository teamRepository ) {
		return new ContactImportProcessor( contactRepository, teamRepository );
	}
}
