package com.foreach.across.samples.valentine.app.jobs;

import com.foreach.across.samples.valentine.app.business.Contact;
import com.foreach.across.samples.valentine.app.business.Team;
import com.foreach.across.samples.valentine.app.repositories.ContactRepository;
import com.foreach.across.samples.valentine.app.repositories.TeamRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Checks that a given contact and team exist and inserts them if required.
 *
 * @author Arne Vandamme
 */
public class ContactImportProcessor implements ItemProcessor<Contact, String>
{
	private static final Logger LOG = LoggerFactory.getLogger( ContactImportProcessor.class );

	private final ContactRepository contactRepository;
	private final TeamRepository teamRepository;

	@Autowired
	public ContactImportProcessor( ContactRepository contactRepository,
	                               TeamRepository teamRepository ) {
		this.contactRepository = contactRepository;
		this.teamRepository = teamRepository;
	}

	@Override
	public String process( Contact contact ) throws Exception {
		if ( StringUtils.isBlank( contact.getFirstName() ) || StringUtils.isBlank( contact.getLastName() ) ) {
			LOG.error( "Could not add contact {} for team {}", contact.getFirstName(), contact.getTeam().getName() );
			return null;
		}

		Team existingTeam = teamRepository.findOneByName( contact.getTeam().getName() );

		if ( existingTeam == null ) {
			existingTeam = new Team();
			existingTeam.setName( contact.getTeam().getName() );
			existingTeam.setImageKey( StringUtils.lowerCase( contact.getTeam().getName() ).replace( ' ', '-' ) );
			existingTeam.setSharingActive( true );
			teamRepository.save( existingTeam );
		}

		Contact existingContact = contactRepository.findOneByLookupKey( contact.getLookupKey() );

		if ( existingContact == null ) {
			existingContact = new Contact();
			existingContact.setFirstName( contact.getFirstName() );
			existingContact.setLastName( contact.getLastName() );
		}
		else {
			existingContact = existingContact.toDto();
		}

		existingContact.setTeam( existingTeam );
		contactRepository.save( existingContact );

		return existingContact.getName();
	}
}
