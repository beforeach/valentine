package com.foreach.across.samples.valentine.app.business;

import com.foreach.across.modules.hibernate.business.SettableIdBasedEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * @author Arne Vandamme
 */
@Entity
@Table(name = "val_team")
public class Team extends SettableIdBasedEntity<Team>
{
	private static final long serialVersionUID = 4401787815278930017L;

	@Id
	@GeneratedValue(generator = "seq_val_team_id")
	@GenericGenerator(
			name = "seq_val_team_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@org.hibernate.annotations.Parameter(name = "sequenceName", value = "seq_val_team_id"),
					@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@NotBlank
	@Size(max = 255)
	@Column(nullable = false, name = "name")
	private String name;

	@Size(max = 255)
	@Column(nullable = true, name = "image_key")
	private String imageKey;

	@Column(name = "sharing_active")
	private boolean sharingActive;

	@Override
	public void setId( Long id ) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getImageKey() {
		return imageKey;
	}

	public void setImageKey( String imageKey ) {
		this.imageKey = imageKey;
	}

	public boolean isSharingActive() {
		return sharingActive;
	}

	public void setSharingActive( boolean sharingActive ) {
		this.sharingActive = sharingActive;
	}
}
