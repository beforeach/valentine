package com.foreach.across.samples.valentine.app.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.samples.valentine.app.business.Contact;
import com.foreach.across.samples.valentine.app.business.Team;
import com.foreach.across.samples.valentine.app.repositories.ContactRepository;
import com.foreach.across.samples.valentine.app.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.util.Arrays;

/**
 * @author Arne Vandamme
 */
@Profile("dev")
@Order(Ordered.LOWEST_PRECEDENCE)
@Installer(description = "Installs some testdata", phase = InstallerPhase.AfterModuleBootstrap)
public class TestDataInstaller
{
	@Autowired
	private ContactRepository contactRepository;

	@Autowired
	private TeamRepository teamRepository;

	@InstallerMethod
	public void installTeamsAndContacts() {
		Team one = new Team();
		one.setName( "Team one" );
		one.setImageKey( "team-one" );
		one.setSharingActive( true );

		Team two = new Team();
		two.setName( "Team two" );
		two.setImageKey( "team-two" );
		two.setSharingActive( false );

		teamRepository.save( Arrays.asList( one, two ) );

		Contact jc = new Contact();
		jc.setFirstName( "Jean-Claude" );
		jc.setLastName( "Van Damme" );
		jc.setTeam( one );

		Contact freddy = new Contact();
		freddy.setFirstName( "Freddy" );
		freddy.setLastName( "Mercury" );
		freddy.setTeam( two );

		contactRepository.save( Arrays.asList( jc, freddy ) );
	}
}
